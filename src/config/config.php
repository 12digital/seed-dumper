<?php
return [

    /**
     * Таблицы для дампа
     */
    'tables' =>
        [
            /**
             * Voyager
             */

            //  'data_rows',
            //  'data_types',
            //  'menu_items',
            //  'menus',
            //  'roles',
            //  'permissions',
            //  'permission_role',
            //  'users',
            //  'translations',
        ],

    /**
     * Maximum number of rows per insert statement
     */
    'chunk_size' => 500,

    /**
     * Path to DatabaseSeeder
     *
     * По умолчанию для Laravel >=8
     * 'path_seeders' => '/database/seeders'
     *
     * По умолчанию для Laravel <8
     * 'path_seeders' => '/database/seeds'
     */

    /**
     * Path to stub file
     *
     * По умолчанию для Laravel >=8
     * 'path_stub' => 'seed8.stub'
     *
     * По умолчанию для Laravel <8
     * 'path_stub' => 'seed.stub'
     */

];
